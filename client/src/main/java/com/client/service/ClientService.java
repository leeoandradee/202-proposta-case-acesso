package com.client.service;

import java.util.Optional;

import com.client.exception.ClientNotFoundExcpetion;
import com.client.model.Client;
import com.client.repository.ClientRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ClientService {

    @Autowired
    private ClientRepository clientRepository;

    public Client saveClient(Client client) {
        return clientRepository.save(client);
    }

    public Client getClientById(int id) {
        Optional<Client> clientOptional = clientRepository.findById(id);
        if (clientOptional.isPresent()) {
            return clientOptional.get();
        } else {
            throw new ClientNotFoundExcpetion();
        }
    }
    
}