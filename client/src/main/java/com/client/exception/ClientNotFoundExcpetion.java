package com.client.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "Client ID nof found")
public class ClientNotFoundExcpetion extends RuntimeException {
    
}