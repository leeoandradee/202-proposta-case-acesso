package com.door.controller;

import com.door.model.Door;
import com.door.service.DoorService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.http.HttpStatus;

@RestController
public class DoorController {

    @Autowired
    private DoorService doorService;

    @PostMapping()
    @ResponseStatus(HttpStatus.CREATED)
    public Door createDoor(@RequestBody Door door) {
        return doorService.saveDoor(door);
    }

    @GetMapping("/{id}")
    public Door findDoorById(@PathVariable(name = "id") int id) {
        return doorService.getDoorById(id);
    }

    
}