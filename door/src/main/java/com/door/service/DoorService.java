package com.door.service;

import java.util.Optional;

import com.door.exception.DoorNotFoundException;
import com.door.model.Door;
import com.door.repository.DoorRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DoorService {

    @Autowired
    private DoorRepository doorRepository;

    public Door saveDoor(Door door) {
        return doorRepository.save(door);
    }
    
    public Door getDoorById(int id) {
        Optional<Door> doorOptional = doorRepository.findById(id);
        if (doorOptional.isPresent()) {
            return doorOptional.get(); 
        } else {
            throw new DoorNotFoundException();
        }
    }
}