package com.door.repository;

import com.door.model.Door;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DoorRepository extends CrudRepository<Door, Integer> {
    
}