package com.access.client;

import org.springframework.context.annotation.Bean;

import feign.Feign;
import feign.RetryableException;
import feign.codec.ErrorDecoder;
import io.github.resilience4j.feign.FeignDecorators;
import io.github.resilience4j.feign.Resilience4jFeign;

public class ClientAcessConfiguration {

    @Bean
    public ErrorDecoder getClientDecoder() {
        return new ClientAccessDecoder();
    }

    @Bean
    public Feign.Builder builder() {
        FeignDecorators decorators = FeignDecorators.builder()
        .withFallback(new ClientAccessFallback(), RetryableException.class)
        .build();

        return Resilience4jFeign.builder(decorators);
    }
    
    
}