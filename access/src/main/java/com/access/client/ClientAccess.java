package com.access.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "client", configuration = ClientAcessConfiguration.class)
public interface ClientAccess {

    @GetMapping("/{clientId}")
    Client getClientById(@PathVariable int clientId);
    
}