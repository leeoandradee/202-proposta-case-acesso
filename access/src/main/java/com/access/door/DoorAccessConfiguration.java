package com.access.door;

import org.springframework.context.annotation.Bean;

import feign.Feign;
import feign.RetryableException;
import feign.codec.ErrorDecoder;
import io.github.resilience4j.feign.FeignDecorators;
import io.github.resilience4j.feign.Resilience4jFeign;

public class DoorAccessConfiguration {

    @Bean
    public ErrorDecoder getDoorDecoder() {
        return new DoorAccessDecoder();
    }

    @Bean
    public Feign.Builder builder() {
        FeignDecorators decorators = FeignDecorators.builder()
        .withFallback(new DoorAccessFallback(), RetryableException.class)
        .build();

        return Resilience4jFeign.builder(decorators);
    }
    
}