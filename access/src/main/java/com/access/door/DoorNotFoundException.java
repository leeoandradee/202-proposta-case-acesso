package com.access.door;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "Door ID nof found")
public class DoorNotFoundException extends RuntimeException{
    
}