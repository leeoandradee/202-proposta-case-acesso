package com.access.door;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "door", configuration = DoorAccessConfiguration.class)
public interface DoorAccess {
    
    @GetMapping("/{doorId}")
    Door getDoorById(@PathVariable int doorId);

}