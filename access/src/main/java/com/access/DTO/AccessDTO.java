package com.access.DTO;

import com.fasterxml.jackson.annotation.JsonGetter;

public class AccessDTO {

    private int doorId;

    private int clientId;

    public AccessDTO() {
    }
    
    @JsonGetter("door_id")
    public int getDoorId() {
        return doorId;
    }

    public void setDoorId(int doorId) {
        this.doorId = doorId;
    }

    @JsonGetter("client_id")
    public int getClientId() {
        return clientId;
    }

    public void setClientId(int clientId) {
        this.clientId = clientId;
    }
    
}