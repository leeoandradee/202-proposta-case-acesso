package com.access.service;

import java.util.Optional;

import com.access.exception.AccessNotFoundException;
import com.access.model.Access;
import com.access.repository.AccessRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AccessService {

    @Autowired
    private AccessRepository accessRepository;

    public Access saveAccess(Access access) {
        return accessRepository.save(access);
    }
    
    public Access getAccessByDoorIdAndClientId(int doorId, int clientId) {
        Optional<Access> accessOptional = accessRepository.findByDoorIdAndClientId(doorId, clientId);
        if (accessOptional.isPresent()) {
            return accessOptional.get();
        } else {
            throw new AccessNotFoundException();
        }
    }
}