package com.access.repository;

import java.util.Optional;

import com.access.model.Access;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AccessRepository extends CrudRepository<Access, Integer>  {

    Optional<Access> findByDoorIdAndClientId(int doorId, int clientId);
    
}