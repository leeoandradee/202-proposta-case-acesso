package com.access.controller;

import com.access.DTO.AccessDTO;
import com.access.client.Client;
import com.access.client.ClientAccess;
import com.access.door.Door;
import com.access.door.DoorAccess;
import com.access.model.Access;
import com.access.service.AccessService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AccessController {

    @Autowired
    private ClientAccess clientAccess;

    @Autowired
    private DoorAccess doorAccess;

    @Autowired
    private AccessService accessService;

    @PostMapping()
    public Access saveAccess(@RequestBody AccessDTO accessDTO) {

        Access access = new Access();
        access.setClientId(accessDTO.getClientId());
        access.setDoorId(accessDTO.getDoorId());

        Client client =  clientAccess.getClientById(access.getClientId());
        Door door = doorAccess.getDoorById(access.getDoorId());

        if (client != null && door != null) {
            return accessService.saveAccess(access);
        } else {
            throw new RuntimeException();
        }
        
    }

    @GetMapping("/{clientId}/{doorId}")
    public Access getAccess(@PathVariable(name = "clientId") int clientId, @PathVariable(name = "doorId") int doorId) {
        return accessService.getAccessByDoorIdAndClientId(doorId, clientId);
    }
    
}